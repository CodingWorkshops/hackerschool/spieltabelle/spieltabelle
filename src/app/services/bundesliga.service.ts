import { Injectable } from '@angular/core';
import { Team } from '../datatypes/team';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import { ConverterService } from './converter.service';
import { map } from 'rxjs/operators';
import { Match } from '../datatypes/match';
import { Tabelle } from '../datatypes/tabelle';


/**
 * Stellt alle wichtigen Schnittstellen bereit, die für die Oberfächenausgaben relevant sind,
 * wie das abrufen der Teams der aktuellen Saison und aller Spieltage der Saison.
 * Außerdem stehen Methoden zum Speichern und laden zur Verfügung
 */
@Injectable({
  providedIn: 'root'
})
export class BundesligaService {

  /**
   * hier werden die Daten für die Punktetabelle gespeichert
   */
  public tabelle: Tabelle = { teams: [] };

  public matches: Match[] = [];

  constructor(private http: HttpClient, private converter: ConverterService) {
    this.refreshTeams(2020).subscribe(teams => this.tabelle.teams = teams);
  }

  /**
   * Aktualisiert die Datenhaltung für die Punktetabelle.
   * Hinzugefügt werden müssen hier noch Berechnung und die Sortierung der Punktetabelle
   * @param matches
   */
  public generateTabelle(matches: Match[]) {
    // TODO
    matches.forEach(match => {
      // sucht das Heimteam aus der Liste "tabelle.teams" heraus und gibt es zurück
      const homeTeam: Team = this.tabelle.teams.find(team => team.name === match.homeTeam);
      // Denke daran es gibt zwei Teams

      // Berechne die Anzahl der eigenen  Tore und Gegentore für das Heim- und Auswärtsteam
      homeTeam.goalsFor += match.goalsHomeTeam;

      // Berechnung der Punkte erfolgt als nächstes
      if (match.goalsHomeTeam > match.goalsAwayTeam) {
      } else if (match.goalsHomeTeam === match.goalsAwayTeam) {


      }
      // Sind das alle Möglichkeiten?
    });

    // Tordifferenz für jedes Team berechnen
    // Schleifen für ein Array siehst du etwas höher


    // teams nach punkten und toren sortieren
    this.tabelle.teams.sort(function (a, b) {
      return 0;
    });
  }

  /**
   * fragt die Teams der akutellen Saison ab und speichert diese in der Variable "tabelle"
   * @param year Jahreszahl für die abzufragende Saison
   */
  public refreshTeams(year: number) {
    return this.getTeams(year + '');
  }

  /**
   * Fragt die Teams der aktuellen Saison ab und reicht diese weiter
   * @param year Jahr der Saison für die, die Teams abgefragt werden soll
   */
  public getTeams(year: string): Observable<Team[]> {
    return this.http.get<Team[]>(`/api/teams/${year}`).pipe(
      map((teamsArr: any[]) => {
        let teams: Team[] = this.converter.convertJsonToTeams(teamsArr);
        return teams;
      })
    );
  }

  /**
   * Fragt die Matches der Spieltage für eine gewünschte Saison ab und gibt diese zurück
   * @param year Jahr der Saison für die, die Spieltage abgefragt werden soll
   */
  public getMatches(year: string): Observable<Match[]> {
    return this.http.get<Match[]>(`/api/matches/${year}`).pipe(
      map((matches: any[]) => {
        this.matches = this.converter.convertJsonToMatches(matches);
        return this.matches;
      })
    );
  }

  /**
   * Über diese Methode können die Spieltage/Matches für eine bestimmte Saison in eine Datei gespeichert werden
   * @param matches Ergebnisse der Spielttage
   * @param year    Jahr der jeweiligen Saison. Die Jahreszahl wird für den Dateinamen verwendet
   */
  public saveMatches(matches: Match[], year: number) {
    let request: any = {
      year: year,
      matches: matches
    };
    this.http.post<Match[]>(`/api/savefile`, request).subscribe();
  }

  /**
   * lädt die Ergebnisse der Spieltage aus einer vorher gespeicherten Datei
   * @param year            Jahreszahl der Saison für die, die gespeicherten Daten abgefragt werden soll
   * @param matchesOutput
   */
  public loadSavedMatchesAndRefreshTabelle(year: number): void {
    this.http.get<Match[]>(`/api/getSavedSeason/` + year).pipe(
      map(result => {
        this.refreshTeams(2019).subscribe(teams => { this.tabelle.teams = teams; this.generateTabelle(result); });
      })
    ).subscribe();
  }
}
