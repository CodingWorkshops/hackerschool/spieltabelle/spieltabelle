import { TestBed } from '@angular/core/testing';

import { BundesligaService } from './bundesliga.service';

describe('BundesligaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BundesligaService = TestBed.get(BundesligaService);
    expect(service).toBeTruthy();
  });
});
