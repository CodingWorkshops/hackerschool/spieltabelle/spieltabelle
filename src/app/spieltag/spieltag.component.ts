import { Component, OnInit } from '@angular/core';

import { Match } from 'src/app/datatypes/match';
import { Tabelle } from 'src/app/datatypes/tabelle';
import { BundesligaService } from 'src/app/services/bundesliga.service';

@Component({
  selector: 'app-spieltag',
  templateUrl: './spieltag.component.html',
  styleUrls: ['./spieltag.component.css']
})
export class SpieltagComponent implements OnInit {

  public matches: Match[] = [];

  constructor(private service: BundesligaService) { }
  ngOnInit() {
    this.service.getMatches('2019').subscribe(res => this.matches = res);
  }

  berechnePunkte(): void {
    this.service.generateTabelle(this.matches);
  }

  // hier müssen noch die Methoden für das Laden und speichern hinzugefügt werden.
  // der Service bietet auch dafür Funktionen


}
