export interface Team {
    name: string;
    shortname: string;
    points: number;
    goalsFor: number;
    goalsAgainst: number;
    diff: number;
}
