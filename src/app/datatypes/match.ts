export interface Match {
    homeTeam: string;
    awayTeam: string;
    goalsHomeTeam: number;
    goalsAwayTeam: number;
    spieltag: number;
}
