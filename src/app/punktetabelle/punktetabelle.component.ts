import { Component, OnInit } from '@angular/core';

import { BundesligaService } from '../services/bundesliga.service';
import { Tabelle } from '../datatypes/tabelle';
import { Team } from '../datatypes/team';


/**
 * 
 */
@Component({
  selector: 'app-punktetabelle',
  templateUrl: './punktetabelle.component.html',
  styleUrls: ['./punktetabelle.component.css']
})
export class PunktetabelleComponent implements OnInit {
  team1: Team = {
    name:"Name des Vereins",
    diff: 2,
    goalsAgainst:2,
    goalsFor:4,
    points:3,
    shortname:"NdV"
  };
  public tabelle: Tabelle;
  
  constructor(private service: BundesligaService) { }

  ngOnInit() {
    //* dies muss später noch angepasst werden. Dies ist nur zu Testzwecken.
    this.tabelle = {teams:[this.team1]};
    this.service.tabelle= this.tabelle;

    //* Die nachfolgende Zeile ist für das holen aller Teams
    // this.tabelle = this.service.tabelle;

    //* Denke daran das du noch die Spieltag Matches benötigst. (service Funktion)
  }


}
