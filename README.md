# Tipps und Tricks
Hier befinden sich ein paar hilfreiche Befehle und Tipps für die Entwicklung der App

## Anwendung starten 
Um die komplette Anwendung zu testen benutze den Befehl `npm run start`. Die Anwendung kann dann unter `http://localhost:9090/` getestet werden.


## Datentypen
Im Verzeichnis "datatypes" befinden sich alle relevanten Datentypen für das Projekt.

## Einbinden der Komponenten in HTML
Die Angular-Komponenten werden unter dem Verzeichnis "main" in der Datei "main.component.html"  eingebunden. Dadurch wird auch die Reihenfolge festgelegt in der die Komponenten angezeigt werden.

## Dokumentationen
Typescript: https://www.typescriptlang.org/docs/handbook/classes.html
Angular:    https://angular.io/start
HTML:		https://wiki.selfhtml.org/wiki/HTML/Tutorials/Einstieg
CSS:		https://wiki.selfhtml.org/wiki/CSS/Tutorials/Einstieg/Syntax 


## Development/Entwicklung des Frontends in GitPod
Führe nur das Pod aus und du kannst loslegen.
Das Frontend wird bei Änderungen automatisch neu kompiliert und ausgeliefert.

## Debug des Clients
Den Client debuggst du am besten im Browser mit den F12 Tools

## Debug des Servers in GitPod
Beende den Node Server (Strg+c im entsprechenden Terminal) und führe die Debug Configuration aus (View -> Debug -> Run Debug Server)

## Test in GitPod
Entweder Testest du mit der Dev Umgebung oder du führst im Hauptverzeichnis npm run Backend aus. Der Client wird dabei über den Server mit ausgeliefert.
