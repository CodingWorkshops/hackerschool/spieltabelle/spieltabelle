import * as request from 'request';
import * as path from 'path';
import * as bodyParser from 'body-parser';
import * as fs from 'fs';
import { Request, Response, Application, Router } from "express";
import * as cors from "cors";
const express = require('express');

// Allowed extensions list can be extended depending on your own needs
const allowedExt = [
  '.js',
  '.ico',
  '.css',
  '.png',
  '.jpg',
  '.woff2',
  '.woff',
  '.ttf',
  '.svg',
];

const allowedAPIs = [
  'teams',
  'matches'
];

const baseUrlBLDB = 'https://www.openligadb.de/api';

var whitelist = ['http://localhost:9090', 'http://localhost:9090/api/matches', 'http://localhost:9090/api/teams']
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { origin: true } // reflect (enable) the requested origin in the CORS response
  } else {
    corsOptions = { origin: false } // disable CORS for this request
  }
  callback(null, corsOptions) // callback expects two parameters: error and options
}
class Server {
  public app: Application;
  public router: Router;
  private port = 9090;

  public static bootstrap(): Server {
    return new Server();
  }

  private static validateYear(year: string): boolean {
    let regexp = new RegExp('^\\d{4}$');
    console.log(year);
    return regexp.test(year);
  }

  private static validateSeason(season: string): boolean {
    let result: boolean = (season && JSON.parse(season));
    // console.log(Object.keys(JSON.parse(season)).length);
    console.log(result);
    return result;
  }

  constructor() {

    // Create expressjs application
    this.app = express();
    // Depending on your own needs, this can be extended
    this.app.use(bodyParser.json({ limit: '50mb' }));
    this.app.use(bodyParser.raw({ limit: '50mb' }));
    this.app.use(bodyParser.text({ limit: '50mb' }));
    this.app.use(bodyParser.urlencoded({
      limit: '50mb',
      extended: true
    }));

    // Route our backend calls
    // Gibt die Teams zurück
    this.app.options('/api/teams/:year', cors(corsOptionsDelegate));
    this.app.get('/api/teams/:year', cors(corsOptionsDelegate), (myRequest: Request, myResponse: Response) => {
      let year: string = myRequest.params['year'];
      if (Server.validateYear(year)) {
        let url = `${baseUrlBLDB}/getavailableteams/bl1/${myRequest.params['year']}`;

        request(url, { json: true }, (err, res, body) => {
          if (err) {
            myResponse.send('Fehler');
            return console.log(err);
          }

          myResponse.status(200).type('json').json(body);
        });
      } else {
        myResponse.status(400).json(`'${year}' entspricht keinem Jahr`);
      }
    });

    // Gibt alle Matches zurück
    this.app.options('/api/matches/:year', cors());
    this.app.get('/api/matches/:year', cors(), (myRequest: Request, myResponse: Response) => {
      let year: string = myRequest.params['year'];
      if (Server.validateYear(year)) {
        let url = `${baseUrlBLDB}/getmatchdata/bl1/${myRequest.params['year']}`;

        request(url, { json: true }, (err, res, body) => {
          if (err) {
            return console.log(err);
          }
          myResponse.json(body);
        });
      } else {
        myResponse.status(400).json(`'${year}' entspricht keinem Jahr`);
      }
    });

    this.app.options('/api/savefile', cors());
    this.app.post('/api/savefile', cors(), (req: Request, res: Response) => {
      let year: string = req.body['year'];
      let matches: string = req.body['matches'];
      console.log("year: " + year);
      console.log("matches: " + matches);
      if (Server.validateYear(year)) {
        fs.writeFile(`seasons/${year}.json`, JSON.stringify(matches), (err) => {
          if (err) {
            console.log(err);
            res.status(501).send('Fehler beim speichern');
          } else {
            res.status(200).send('{"result": "Saison gespeichert"}');
          }
        });
      } else {
        res.status(400)
          .json({
            year: year,
            matches: matches,
            message: 'Fehlerhafte Werte'
          });
      }
    });

    this.app.options('/api/getAllSavedSeasons', cors());
    this.app.get('/api/getAllSavedSeasons', cors(), (req: Request, res: Response) => {
      fs.exists('seasons', (isExists: boolean) => {
        if (isExists) {
          fs.readdir('seasons', (err, files) => {
            if (err) {
              console.log(err);
              res.status(501).statusMessage = 'Fehler beim lesen';
              res.send();
            } else {
              let seasons: string[] = [];
              for (let filename of files) {
                filename = filename.replace(".json", "");
                seasons.push(filename);
              }
              res.status(200).json({ seasons: seasons });
            }
          });
        } else {
          res.status(200).send([]);
        }
      });
    });

    this.app.options('/api/getSavedSeason/:year', cors());
    this.app.get('/api/getSavedSeason/:year', cors(), (req: Request, res: Response) => {
      console.log("load Data");
      let year: string = req.params['year'];
      try {
        fs.exists('./seasons/' + year + ".json", (isExists: boolean) => {
          if (isExists) {
            fs.readFile('./seasons/' + year + '.json', (err, rawdata) => {
              console.log("" + rawdata);
              res.status(200).send(rawdata);
            });
          } else {
            res.status(501).send('Saison nicht gefunden');
          }
        });
      } catch (err) {
        console.log(err);
        res.status(501).send('Fehler beim laden');
      }
    });

    // Redirect all the other resquests
    this.app.get('*', cors(), (req: Request, res: Response) => {
      if (allowedExt.filter(ext => req.url.indexOf(ext) > 0).length > 0) {
        console.log(req.url);
        res.sendFile(path.resolve(`dist/bundesliga-tabelle-app/${req.url}`));
      } else {
        res.sendFile(path.resolve('dist/bundesliga-tabelle-app/index.html'));
        console.log("send index.html");
      }
    });

    // Start the server on the provided port
    this.app.listen(this.port, () => console.log(`http is started ${this.port}`));
  }
}

//Bootstrap the server, so it is actualy started
const server = Server.bootstrap();
export default server.app;
